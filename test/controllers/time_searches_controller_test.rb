require 'test_helper'

class TimeSearchesControllerTest < ActionController::TestCase
  setup do
    @time_search = time_searches(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:time_searches)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create time_search" do
    assert_difference('TimeSearch.count') do
      post :create, time_search: {  }
    end

    assert_redirected_to time_search_path(assigns(:time_search))
  end

  test "should show time_search" do
    get :show, id: @time_search
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @time_search
    assert_response :success
  end

  test "should update time_search" do
    patch :update, id: @time_search, time_search: {  }
    assert_redirected_to time_search_path(assigns(:time_search))
  end

  test "should destroy time_search" do
    assert_difference('TimeSearch.count', -1) do
      delete :destroy, id: @time_search
    end

    assert_redirected_to time_searches_path
  end
end
