$(document).ready(function () {

    $("#transaction_total_price, #transaction_tran_qty").keyup(function () {
        total_price = parseFloat($("#transaction_total_price").val());
        tran_qty = parseFloat($("#transaction_tran_qty").val());
        amount_owed = total_price * tran_qty;
        $("#amount_owed").val(amount_owed);
    })
});