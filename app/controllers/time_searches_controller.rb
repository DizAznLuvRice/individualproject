class TimeSearchesController < ApplicationController
  before_action :set_time_search, only: [:show, :edit, :update, :destroy]

  # GET /time_searches
  # GET /time_searches.json
  def index
    @time_searches = Search.all
  end

  # GET /time_searches/1
  # GET /time_searches/1.json
  def show
  end

  # GET /time_searches/new
  def new
    @time_search = Search.new
  end

  # GET /time_searches/1/edit
  def edit
  end

  # POST /time_searches
  # POST /time_searches.json
  def create
    @time_search = Search.new(time_search_params)

    respond_to do |format|
      if @time_search.save
        format.html { redirect_to @time_search, notice: 'Time search was successfully created.' }
        format.json { render action: 'show', status: :created, location: @time_search }
      else
        format.html { render action: 'new' }
        format.json { render json: @time_search.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /time_searches/1
  # PATCH/PUT /time_searches/1.json
  def update
    respond_to do |format|
      if @time_search.update(time_search_params)
        format.html { redirect_to @time_search, notice: 'Time search was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @time_search.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /time_searches/1
  # DELETE /time_searches/1.json
  def destroy
    @time_search.destroy
    respond_to do |format|
      format.html { redirect_to time_searches_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_time_search
      @time_search = Search.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def time_search_params
      params[:time_search]
    end
end
