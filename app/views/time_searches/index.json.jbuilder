json.array!(@time_searches) do |time_search|
  json.extract! time_search, :id
  json.url time_search_url(time_search, format: :json)
end
