json.array!(@searches) do |search|
  json.extract! search, :id, :transaction_id, :item_id, :item_name, :quantity, :start_date, :end_date
  json.url search_url(search, format: :json)
end
