json.array!(@transactions) do |transaction|
  json.extract! transaction, :id, :item_id, :date, :total_price, :tran_qty
  json.url transaction_url(transaction, format: :json)
end
