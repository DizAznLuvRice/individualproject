class Item < ActiveRecord::Base
  has_many :transactions
  has_many :searches, through: :transaction
end
