class Report < ActiveRecord::Base
  belongs_to :transaction
  belongs_to :item

  def items
    @items ||= find_items
  end

  def transactions
    @transactions ||= find_transaction
  end


  def find_items
    items = items.order(:name)
    items = items.where("quantity <= ?",quantity)
  end

  def find_transactions
    transactions = transactions.order(:sale_date)
    transactions = transactions.where("date >= ?", start_date)
    transactions = transactions.where("date <= ?", end_date)
  end
end
