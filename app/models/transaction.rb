class Transaction < ActiveRecord::Base

  has_many :items
  has_many :searches, through: :items


  scope :gray, lambda {order('id ASC')}
  after_save :update_today


def update_today
  transaction_quantity = Transaction.gray.last.tran_qty
  item_quantity = Item.find(Transaction.gray.last.item_id)
  tran_num = item_quantity.quantity
  order_total = (tran_num - transaction_quantity)
end

end