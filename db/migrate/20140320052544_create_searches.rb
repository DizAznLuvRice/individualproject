class CreateSearches < ActiveRecord::Migration
  def change
    create_table :searches do |t|
      t.integer :transaction_id
      t.integer :item_id
      t.string :item_name
      t.integer :quantity
      t.date :start_date
      t.date :end_date

      t.timestamps
    end
  end
end
