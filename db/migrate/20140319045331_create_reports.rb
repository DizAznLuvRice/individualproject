class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.integer :item_id
      t.integer :transaction_id

      t.timestamps
    end
  end
end
