class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.integer :item_id
      t.date :date
      t.float :total_price
      t.integer :tran_qty

      t.timestamps
    end
  end
end
